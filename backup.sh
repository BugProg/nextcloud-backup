#!/bin/bash

#Git : https://gitlab.com/BugProg/nextcloud-backup

######################## DESCRIPTION ########################
# Backup files, folder and Database easily !


# dependencies to send an email :
# - mailutils
# - msmtp 
# - msmtp-mta
# - bsd-mailx


filesBackup=0

foldersBackup=0

DBsbackup=0

#The user email to send mail
#email=''


# Files directories configuration
#   ['From']="To"
readonly -A filesToSave=( 
#   ['/directory/file.file']="folder/directory/"
  )

# Folders directories configuration
readonly -A foldersToSave=(
#   ['folder/to/save']="folder/directory/"
  )

#DB (MYSQL and MARIADB only)

declare -A DBToSave
DBToSave[0,'host']="localhost"
DBToSave[0,'user']="DB_user"
DBToSave[0,'DBname']="DB_name"
DBToSave[0,'targetDirectory']="folder/"
DBToSave[0,'group-suffix']="1"


#This commands will be execute before the backup.
readonly -a commandToExecuteBefore=(
  #"sudo systemctl stop apache2"
)
echo ${!commandToExecuteBefore[@]}
#This commands will be execute after the backup.
readonly -a commandToExecuteAfter=(
  #"sudo reboot"
)

#The path for the common logs
logDirectory="/var/log/backupNextcloudDolibarr.log"

#The path for rsync logs
logRsyncDirectory="/var/log/backupNextcloudDolibarr_rsync.log"

###############################################
#            !!!! END OF CONF !!!!
###############################################

writte_into_logFile ()
{
  #DESCRIPTION :
  # Writte log into the logs files
  #
  # PARAMETERS :
  # - error in $1 if it's an error
  # - the name of the operation
  # - The message
  # Example :  writte_into_logFile error logFiles "File $logDirectory not create."
  #
  # OUTPUT :
  # - none
  #

  if [ "$1" = "error" ]
  then
    echo "1 $2 : [`date +"%c"`] $3 " >> ${logDirectory}
    echo "Exit with error 1" >> ${logDirectory}
    send_mail "Rapport Backup Nextcloud (erreur)" "Quelque chose c'est mal passé." ${logDirectory}
    exit 1
  else
    echo "0 $1 : [`date +"%c"`] $2 " >> ${logDirectory}
  fi
}

send_mail ()
{
    #DESCRIPTION :
  # Send mail to user
  #
  # PARAMETERS :
  # - $1 The subject
  # - $2 The message
  # - $3 The file to link with the mail
  #
  # OUTPUT :
  # - none
  
  # echo "coucou" + cat ${logDirectory} |tail -n20 | mail -s "Rapport Backup Nextcloud (erreur)" ${email}
  test -z $email || echo $2 | cat - $3 | mail -s $1 ${email}
}

#Create log files
touch ${logDirectory}; touch ${logRsyncDirectory} || writte_into_logFile "error" "LogFiles" "File $logDirectory or $logRsyncDirectory not create."

#Execute the command defined by the user before the backup
for i in ${!commandToExecuteBefore[@]}
  do
    bash -c "${commandToExecuteBefore[$i]}" || writte_into_logFile "error" "bash error" "Failed to execute : '${commandToExecuteBefore[$i]}'."
  done

#Files
if [[ $filesBackup == 1 ]]
then
  for i in ${!filesToSave[@]}
  do
    if [[ ! -f $i ]] #If the source file doesn't exist
    then
      writte_into_logFile "error" "rsyncBackupFiles" "The source ${i} doesn't exist !"

    elif [[ ! -d ${filesToSave[$i]} ]] #Test if the target directory, else creat it
    then
      mkdir -p ${filesToSave[$i]} || writte_into_logFile "error" "Create backup directory failed" "Path ${filesToSave[$i]} not create."

    else #Start the backup
      rsync -Ax --log-file=$logRsyncDirectory $i ${filesToSave[$i]} && writte_into_logFile "rsyncBackupFiles" "${i} saved !"
    fi
  done
fi

#Folder
if [[ $foldersBackup == 1 ]]
then
  for i in ${!foldersToSave[@]}
  do
    if [[ ! -d $i ]] #If the source directory doesn't exist
    then
      writte_into_logFile "error" "rsyncBackupFolder" "The source ${i} doesn't exist !"

    elif [[ ! -d ${foldersToSave[$i]} ]] #Test if the target directory, else creat it
    then
      mkdir -p ${foldersToSave[$i]} || writte_into_logFile "error" "Create backup directory failed" "Path ${foldersToSave[$i]} not create."
      
    else #Start the backup
      rsync -Aax --log-file=$logRsyncDirectory --delete-after $i ${foldersToSave[$i]} && writte_into_logFile "rsyncBackupFolder" "${i} saved !"
    fi
  done
fi

#DataBase
if [[ $DBsbackup == 1 ]]
then
  for i in $(seq 0 $(( (${#DBToSave[@]} / 5) - 1)))
  do
    mysqldump --defaults-group-suffix=${DBToSave[$i,'group-suffix']} --single-transaction -h ${DBToSave[$i,'host']} -u ${DBToSave[$i,'user']}  ${DBToSave[$i,'DBname']} > ${DBToSave[$i,'targetDirectory']}/${DBToSave[$i,'DBname']}_`date +"%Y_%m_%d"`.bak && writte_into_logFile "DBsave" "${DBToSave[$i,'DBname']} saved !" || writte_into_logFile "error" "DBsave" "Faild to backup ${DBToSave[$i,'DBname']}."
  done
fi

send_mail "Rapport Backup Nextcloud" "La sauvegarde c'est correctement déroulé"

#Commmands to execute after the backup
for i in ${!commandToExecuteAfter[@]}
  do
    bash -c "${commandToExecuteAfter[$i]}" || writte_into_logFile "error" "bash error" "Failed to execute : '${commandToExecuteAfter[$i]}'."
  done

