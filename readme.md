## WebSite Backup utilities

Backup easily your files, folders and database.

## How to install

Clone the current repository in the root directory.

Make sure you have the execution permissions :```sudo chmod +x backup.sh```

##### Add files to the backup :

A line start with ```readonly -A filesToSave=(```, you can add your own file with the syntax : ```['sourceFile.file']="TargetDirectory"```

You can add another file like :

```bash
['sourceFile.file']="TargetDirectory/"
['anotherSourceFile.file']="anotherTargetDirectory/"
```

##### Add folders to the backup :

It's exactly the same configuration for the files.

A line start with `readonly -A foldersToSave=(`, you can add your own file with the syntax : `['sourceFolder']="TargetDirectory"`

You can add another file like :

```bash
['source/Folder']="Target/Directory/"
['source/Folder']="another/Target/Directory/"
```

**!!! Be careful !!! Don't forget the '/' at the end of the the target directory and do not put any '/' for the source directory !!!**

##### Add data base to the backup :

In the first time the file `.my.cnf` must be create in `/root/` to connect to the database without enter password.

Edit the files and add :

```bash
[client1]
user=db_user
password=passwordDB

[client2]
user=db_user2
password=passwordDB

[client3]
user=db_user3
password=passwordDB
```

This will alow to backup.sh to dump the data base without enter pawword.

 Don't forget to give the right permissions with : `chmod 600 .my.cnf`



Now, you can edit the backup.sh file and go to `declare -A DBToSave`.

You will find this :

```bash
DBToSave[0,'host']="localhost"
DBToSave[0,'user']="db_user"
DBToSave[0,'DBname']="dataBaseName"
DBToSave[0,'targetDirectory']="folder/directory"
DBToSave[0,'group-suffix']="1"
```

Enter the relevant informations for :

`host -> the ip of the server`

`user -> The user of the database`

`DBname -> The name of the DB`

`targetDirectory -> The backup directory`

`group-suffix -> Corresponds to the client number in the file .my.cnf`



If you want to backups two or more DB just copy the lines and change the 0 to 1 :

```bash
DBToSave[0,'host']="localhost"
DBToSave[0,'user']="db_user"
DBToSave[0,'DBname']="dataBaseName"
DBToSave[0,'targetDirectory']="folder/directory"
DBToSave[0,'group-suffix']="1"

DBToSave[1,'host']="localhost"
DBToSave[1,'user']="another_db_user"
DBToSave[1,'DBname']="another_dataBaseName"
DBToSave[1,'targetDirectory']="another/folder/directory"
DBToSave[1,'group-suffix']="2"
```

Don't forget to swith the var `DBsbackup` to `1` to enable the backup.



##### Send mail

An email can be sent if the operation was completed successfully or not.

Dependencies :

```
mailutils
msmtp 
msmtp-mta
bsd-mailx
```

For the configuration of the client see the links below.

[msmtp [Wiki ubuntu-fr]](https://doc.ubuntu-fr.org/msmtp)

[Send emails from your terminal with msmtp](https://arnaudr.io/2020/08/24/send-emails-from-your-terminal-with-msmtp/)



In the backup.sh, find the var `email` uncomment it and writte you email.

That's all !!
